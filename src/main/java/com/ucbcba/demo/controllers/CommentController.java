package com.ucbcba.demo.controllers;

import com.ucbcba.demo.entities.Comment;
import com.ucbcba.demo.entities.Post;
import com.ucbcba.demo.services.CommentService;
import com.ucbcba.demo.services.CommentServiceImpl;
import com.ucbcba.demo.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

@Controller
public class CommentController {
    private PostService postService;
    private CommentService commentService;

    @Autowired
    public void setCommentService(CommentService commentService) {
        this.commentService = commentService;
    }

    @RequestMapping(value = "/comment", method = RequestMethod.POST)
    String save(@Valid Comment comment, BindingResult bindingResult,Model model) {

        if (bindingResult.hasErrors()) {
            //Post post = postService.getPost(comment.getPost().getId());
            //model.addAttribute("post", post);
            model.addAttribute("comment",new Comment());
            model.addAttribute("commentList", commentService.listAllComments());
            return "redirect:/post/" + comment.getPost().getId();
        }
        commentService.saveComment(comment);
        return "redirect:/post/" + comment.getPost().getId();
    }

    @RequestMapping("/commentlike/{id}")
    String commentLike(@PathVariable Integer id) {
        Comment comment = commentService.getComment(id);
        comment.setLikes(comment.getLikes()+1);
        commentService.saveComment(comment);
        return "redirect:/post/"+ comment.getPost().getId();
    }

    @RequestMapping("/commentDislike/{id}")
    String commentDislike(@PathVariable Integer id) {
        Comment comment = commentService.getComment(id);
        if(comment.getLikes()>0)
            comment.setLikes(comment.getLikes()-1);
        commentService.saveComment(comment);
        return "redirect:/post/"+ comment.getPost().getId();
    }
}
