package com.ucbcba.demo.controllers;

import com.ucbcba.demo.entities.Comment;
import com.ucbcba.demo.entities.Post;
import com.ucbcba.demo.entities.PostCategory;
import com.ucbcba.demo.repository.UserRepository;
import com.ucbcba.demo.services.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

@Controller
public class PostController {
    private PostService postService;
    private PostCategoryService postCategoryService;
    private UserService userService;
    private CommentService commentService;

    @Autowired
    public void setPostCategoryService(PostCategoryService productService) {
        this.postCategoryService = productService;
    }

    @Autowired
    public void setCommentService(CommentService commentService) {
        this.commentService = commentService;
    }

    @Autowired
    public void setPostService(PostService productService) {
        this.postService = productService;
    }

    @Autowired
    public void setUserService(UserService productService) {
        this.userService = productService;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String showAll(Model model) {
        Iterable<Post> postList = postService.listAllPosts();
        model.addAttribute("postList",postList);
        model.addAttribute("commentList", commentService.listAllComments());
        model.addAttribute("userList",userService.listAllUsers());
        return "showAll";
    }

    @RequestMapping(value = "/posts", method = RequestMethod.GET)
    public String list(Model model) {
        Iterable<Post> postList = postService.listAllPosts();
        model.addAttribute("variableTexto","Hello world");
        model.addAttribute("postList",postList);
        return "posts";
    }

    @RequestMapping(value="/newPost", method = RequestMethod.GET)
    String newPost(Model model) {
        model.addAttribute("postCategoryList",postCategoryService.listAllCategories());
        model.addAttribute("userList",userService.listAllUsers());
        model.addAttribute("post", new Post());
        return "newPost";
    }

    @RequestMapping(value = "/post", method = RequestMethod.POST)
    String save(@Valid Post post, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("postCategoryList",postCategoryService.listAllCategories());
            model.addAttribute("userList",userService.listAllUsers());
            return "newPost";
        }
        postService.savePost(post);
        return "redirect:/posts";
    }

    @RequestMapping(value = "/postEdit", method = RequestMethod.POST)
    String saveEdit(@Valid Post post, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("post", post);
            model.addAttribute("postCategoryList",postCategoryService.listAllCategories());
            model.addAttribute("userList",userService.listAllUsers());
            return "edit";
        }
        postService.savePost(post);
        return "redirect:/posts";
    }

    @RequestMapping("/post/{id}")
    String show(@PathVariable Integer id, Model model) {
        model.addAttribute("comment", new Comment());
        Post post = postService.getPost(id);
        model.addAttribute("post", post);
        model.addAttribute("commentList", commentService.listAllComments());
        model.addAttribute("userList",userService.listAllUsers());
        return "show";
    }

    @RequestMapping("/editPost/{id}")
    String editPost(@PathVariable Integer id,Model model) {
        Post post = postService.getPost(id);
        model.addAttribute("post", post);
        model.addAttribute("postCategoryList",postCategoryService.listAllCategories());
        model.addAttribute("userList",userService.listAllUsers());
        return "edit";
    }

    @RequestMapping("/deletePost/{id}")
    String delete(@PathVariable Integer id) {
        postService.deletePost(id);
        return "redirect:/posts";
    }

    @RequestMapping("/like/{id}")
    String like(@PathVariable Integer id) {
        Post post = postService.getPost(id);
        post.setLikes(post.getLikes()+1);
        postService.savePost(post);
        return "redirect:/post/{id}";
    }

    @RequestMapping("/dislike/{id}")
    String dislike(@PathVariable Integer id) {
        Post post = postService.getPost(id);
        if(post.getLikes()>0)
         post.setLikes(post.getLikes()-1);
        postService.savePost(post);
        return "redirect:/post/{id}";
    }

    @RequestMapping(value = "/addCategoryToPost/{id}", method = RequestMethod.GET)
    public String addCategoryToPost(@PathVariable Integer id, Model model) {
        Iterable<PostCategory> postCategoryList = postCategoryService.listAllCategories();
        Post post = postService.getPost(id);
        model.addAttribute("post",post);
        model.addAttribute("postCategoryList",postCategoryList);
        return "chooseCategory";
    }
}
