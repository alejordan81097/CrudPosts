package com.ucbcba.demo.repository;

import com.ucbcba.demo.entities.Comment;
import org.springframework.data.repository.CrudRepository;

public interface CommentRepository extends CrudRepository<Comment,Integer> {
}
