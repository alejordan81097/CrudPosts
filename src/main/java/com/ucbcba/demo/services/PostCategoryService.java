package com.ucbcba.demo.services;
import com.ucbcba.demo.entities.PostCategory;

public interface PostCategoryService {

    Iterable<PostCategory> listAllCategories();

    PostCategory getCategoryById(Integer id);

    PostCategory saveCategory(PostCategory category);

    void deleteCategory(Integer id);

}
