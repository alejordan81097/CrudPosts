package com.ucbcba.demo.services;

import com.ucbcba.demo.entities.PostCategory;
import com.ucbcba.demo.repository.PostCategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class PostCategoryServiceImpl implements PostCategoryService {

    private PostCategoryRepository categoryRepository;

    @Autowired
    @Qualifier(value = "postCategoryRepository")
    public void setCategoryRepository(PostCategoryRepository postCategoryRepository) {
        this.categoryRepository = postCategoryRepository;
    }

    @Override
    public Iterable<PostCategory> listAllCategories() {
        return categoryRepository.findAll();
    }

    @Override
    public PostCategory getCategoryById(Integer id) {
        return categoryRepository.findById(id).get();
    }

    @Override
    public PostCategory saveCategory(PostCategory category) {
        return categoryRepository.save(category);
    }

    @Override
    public void deleteCategory(Integer id) {
        categoryRepository.deleteById(id);
    }

}
