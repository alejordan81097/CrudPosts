package com.ucbcba.demo.services;

import com.ucbcba.demo.entities.Post;
import com.ucbcba.demo.entities.PostCategory;

public interface PostService {

    Iterable<Post> listAllPosts();

    void savePost(Post post);

    Post getPost(Integer id);

    Post editPost(Integer id);

    void deletePost(Integer id);

}
