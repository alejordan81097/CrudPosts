delete from post_category;
delete from post;
delete from user;
delete from comment;

INSERT INTO post_category(id, name) VALUES (1000, 'Deportes');
INSERT INTO post_category(id, name) VALUES (1001, 'Noticias');

INSERT INTO user(id, name) VALUES(1,'Miguel');
INSERT INTO user(id, name) VALUES(2,'Alejandro');

INSERT INTO post(id, title, text, post_category_id, user_id,show_post) VALUES (2000,'Hola' , 'Post 1', 1000, 1,'SI');
INSERT INTO post(id, title, text, post_category_id, user_id,show_post) VALUES (2002,'Hola2', 'Post 2', 1001, 2,'SI');

INSERT INTO comment(id,text,post_id,user_id) VALUES (1,'hola',2000,1);
INSERT INTO comment(id,text,post_id,user_id) VALUES (2,'hola2',2002,2);
INSERT INTO comment(id,text,post_id,user_id) VALUES (3,'chau',2000,1);
INSERT INTO comment(id,text,post_id,user_id) VALUES (4,'chau2',2002,2);

